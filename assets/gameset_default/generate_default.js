const fs = require('fs');
const imagemin = require("imagemin");                                         
const imageminPngquant = require("imagemin-pngquant");              

// Definining the units of measurement
const s = 1;
const min = 60 * s;
const cm = 1;
const m = 10 * cm;

const pointsNumber = 30;

const trackLength = 10 * cm;

function times(maxTime) {
    // Times should always be evenly spaced
    // Subtracting 1 to the dimension, so the last point corresponds to maxTime
    return Array(pointsNumber).fill(0).map((v, i) => maxTime * i / (pointsNumber - 1));
}

// Using a reproducible pseudo-random generator to generate level codes
const M = 2147483648; // 2^31
const a = 1103515245;
const c = 12345;
var seed = 3;
function pseudorandom() {
    seed = (a * seed + c) % M;
    return seed / M;
}

function generateCode() {
    const codeLength = 11;
    // Not including confusing characters like Il1 or 0O
    const characters = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvw123456789";

    return Array(codeLength).fill(0).map(v => characters[Math.floor(pseudorandom() * characters.length)]).join("");
}

async function minifyImage(path) {
    const files = await imagemin([path], {
        plugins: [imageminPngquant({quality: [0.5, 0.6]})]
    });

    const prefix = "data:image/png;base64,"

    return prefix + files[0].data.toString('base64');
}

(async function () {
let output = {};
output.name = "default";
output.questions = [];

let maxTime = 10 * s;
let V = trackLength / (9.7 * s);
output.questions.push({
    image: await minifyImage("question1.png"),
    imageType: "base64",
    code: generateCode(),
    trackLength: trackLength,
    maxTime: maxTime,
    positions: [times(maxTime).map((t, i) => 1.05 * cm / s * t),
                times(maxTime).map((t, i) => 1.05 * cm / s * t - 0.5 * cm),
                times(maxTime).map((t, i) => V * t)
    ]
});

output.questions.push({
    image: await minifyImage("question2.png"),
    imageType: "base64",
    code: generateCode(),
    trackLength: trackLength,
    maxTime: maxTime,
    positions: [times(maxTime).map((t, i) => 1 * cm / s * t),
                times(maxTime).map((t, i) => 0.5 * 0.1 * cm / (s * s) * t * t + 1 * cm / s * t),
                times(maxTime).map((t, i) => -0.5 * 0.03 * cm / (s * s) * t * t + 1 * cm / s * t)
    ]
});

maxTime = 6 * s;
V = trackLength / (5.4 * s);
output.questions.push({
    image: await minifyImage("question3.png"),
    imageType: "base64",
    code: generateCode(),
    trackLength: trackLength,
    maxTime: maxTime,
    positions: [times(maxTime).map((t, i) => 0.5 * 0.4 * cm / (s * s) * t * t + 1.2 * cm / s * t),
                times(maxTime).map((t, i) => 0.57 * cm / (s * s) * t * t + 1.2 * cm / s * t - 5 * cm),
                times(maxTime).map((t, i) => V * t)
    ]
});

maxTime = 9 * s;
output.questions.push({
    image: await minifyImage("question4.png"),
    imageType: "base64",
    code: generateCode(),
    trackLength: trackLength,
    maxTime: maxTime,
    positions: [times(maxTime).map((t, i) => 1.3 * cm / s * t),
                times(maxTime).map((t, i) => 0.5 * -0.03 * cm / (s * s) * t * t + 1.2 * cm / s * t),
                times(maxTime).map((t, i) => 0.5 * 0.3 * cm / (s * s) * t * t + 0.5 * cm / s * t)
    ]
});

maxTime = 10 * s;
output.questions.push({
    image: await minifyImage("question5.png"),
    imageType: "base64",
    code: generateCode(),
    trackLength: trackLength,
    maxTime: maxTime,
    positions: [times(maxTime).map((t, i) => 0.5 * 0.37 * cm / (s * s) * t * t + 0.2 * cm / s * t),
                times(maxTime).map((t, i) => 9 * cm * Math.sin(Math.PI * t / (8.5 * s))),
                times(maxTime).map((t, i) => 0.5 * -0.09 * cm / (s * s) * t * t + 1.9 * cm / s * t)
    ]
});

fs.writeFileSync("../../build/gameset_default.json", JSON.stringify(output, null, 4));
})()
