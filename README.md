# Maggots races

Didactic racing game about kinematics

* [Introduction](#introduction)
* [Published version](#published-version)
* [Running a local copy](#running-a-local-copy)
* [Bibliography](#bibliography)

## Introduction
When approaching kinematics for the first time, students might encounter the difficulty of understanding the subject's different languages [1, 2].
Teachers tend to take for granted the steps necessary to translate the descriptions of motion between the verbal language, the mathematical language (_i.e._ equations), and the graphical language (_i.e._ space-time graphs, photographs with strobe lights, _etc._).
Another aspect, that involves difficulties, is the connection between these descriptions and the perception of the reality of motion [3].

With this videogame, we want to aid the students in their understanding of the different languages and the connections between them.
The game shows the kinematic description of the motion of three maggots along a linear track.
The student will be asked to calculate and then indicate the order of arrival of the larvae.
The description of the motion is provided in one of the languages (verbal, mathematical, and graphical).
Students are therefore invited to interpret all languages and apply the mathematical formalism necessary to deduce the arrival order.
The playful component can stimulate the classes and thus help the understanding.

## Report for teachers
In the summary screen there is the possibility of generating a report for the teacher.
The report is a PNG image with an [Aztec Code](https://en.wikipedia.org/wiki/Aztec_Code).
The report information is encoded in the Aztec Code as a JSON string.
It contains:
- the gameset name;
- the list of answers;
- the percentage of correct answers;
- a timestamp;
- the version of the game;
- a hash code with the previous information.

## Published version
The game has been published to: https://fontana.itch.io/maggots-races

## Running a local copy
For convenience, we use `npm` to install the dependencies and to run the web-server necessary for the game.
To run a local copy of the game run the following commands:

```bash
npm install
npm run build
npm run start
```

## Acknowledgements
* Thanks to [Joeb Rogers](https://joebrogers.com/) for the great font [BitPotion](https://joebrogers.itch.io/bitpotion).
* Thanks to [mp0](https://itch.io/profile/mp0) for the [7x7 Pixel Font](https://mp0.itch.io/pixel-font).

## Bibliography
[1] Trowbridge DE and McDermott LC, American Journal of Physics 48, 1020 (1980), _“Investigation of student understanding of the concept of velocity in one dimension”_, [DOI:10.1119/1.12298](https://doi.org/10.1119/1.12298).

[2] Trowbridge DE and McDermott LC, American Journal of Physics 49, 242 (1981), _“Investigation of student understanding of the concept of acceleration in one dimension”_, [DOI:10.1119/1.12525](https://doi.org/10.1119/1.12525).

[3] Arons AB, John Wiley & Sons, 1st ed. (1996), _“Teaching Introductory Physics”_, ISBN-13:978-0471137078.
