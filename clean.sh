#! /bin/bash -x

rm public/js/phaser.js
rm public/js/game.js
rm public/assets/gamesets/default.json
rm public/assets/sprites/atlas_main.png
rm public/assets/sprites/atlas_main.json
rm public/assets/fonts/pixel7x7.xml
rm public/assets/fonts/pixel7x7.png
rm public/assets/fonts/scoreboard.xml
rm public/assets/fonts/scoreboard.png
rm public/assets/fonts/keyboard.xml
rm public/assets/fonts/keyboard.png
rm build/*
