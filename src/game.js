// (C) Copyright 2021 Cristiano Lino Fontana
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

"use strict";

/**************************************************************************/
/* Utilities                                                              */
/**************************************************************************/
function zeroPad(number, width)
{
    const s = number.toString();
    width -= s.length;

    if (width > 0) {
        return new Array(width).fill(0).join('') + s;
    } else {
        return s;
    }
}

function downloadFile(fileName, dataURL, mimeType) {
    let a = document.createElement('a');
    a.download = fileName;
    a.href = dataURL;
    a.textContent = "Download " + fileName;
    a.dataset.downloadurl = [mimeType, a.download, a.href].join(':');
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}

function hashCode(str) {
    // Code from: https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript
    var hash = 0, i, chr;
    for (i = 0; i < str.length; i++) {
        chr   = str.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
}


/**************************************************************************/
/* Game settings                                                          */
/**************************************************************************/
var game = null;

const gameSettings = {
    aspectRatio: 9.0/16,
    width: 360,
    height: 640,
    background: 0x000000,
    progressBar: {
        height: 32,
        width: 300,
        padding: 5,
        margin: 20,
        barColor: 0xaaaaaa,
        boxColor: 0x222222,
        textColor: '#ffffff',
        textStyle: '20px monospace'
    },
    mainScreen: {
        titlegraphics:   { x: 180, y:  20, x0: 0.5, y0: 0 },
        buttonStart:     { x: 180, y: 230, x0: 0.5, y0: 0 },
        clickHere:       { x:  65, y: 336, x0:   0, y0: 0 },
        buttonLevel:     { x: 180, y: 460, x0: 0.5, y0: 0 },
        copyrightNotice: { x:  13, y: 616, x0:   0, y0: 0, size: 16, text: "(C) C.L. Fontana licensed under CC BY-NC-SA"},
        versionText:     { x: 348, y: 630, x0:   1, y0: 1, size: 16, text: "v0.3.0"}
    },
    loadLevelScreen: {
        loadingLevel:    { x: 180, y: 320, x0: 0.5, y0: 0.5 },
    },
    questionScreen: {
        labelChoose:     { x: 180, y:   0, x0: 0.5, y0: 0 },
        buttonsChoose:  [{ x:  26, y:  42, x0:   0, y0: 0 },
                         { x: 131, y:  43, x0:   0, y0: 0 },
                         { x: 246, y:  42, x0:   0, y0: 0 },
        ],
        question:        { x:  11, y:  95, x0:   0, y0: 0 },
        questionText:    { x: 348, y: 630, x0:   1, y0: 1, size: 16, text: "Question "}
    },
    track: {
        raceStart:       { x: 180, y: 480, x0: 0.5, y0: 0.5},
        advertisements:  { x:   0, y:   0, x0:   0, y0: 0 },
        grassTop:        { x:   0, y:  32, x0:   0, y0: 0 },
        grassBottom:     { x:   0, y: 160, x0:   0, y0: 0 },
        lane1:           { x:   0, y: 128, x0:   0, y0: 0 },
        lane2:           { x:   0, y:  96, x0:   0, y0: 0 },
        lane3:           { x:   0, y:  64, x0:   0, y0: 0 },
        laneNumber1a:    { x:   8, y: 128, x0:   0, y0: 0 },
        laneNumber1b:    { x: 261, y: 128, x0:   0, y0: 0 },
        laneNumber2a:    { x:  24, y:  96, x0:   0, y0: 0 },
        laneNumber2b:    { x: 277, y:  96, x0:   0, y0: 0 },
        laneNumber3a:    { x:  40, y:  64, x0:   0, y0: 0 },
        laneNumber3b:    { x: 293, y:  64, x0:   0, y0: 0 },
        maggots:        [{ x:  -6, y: 118, x0:   0, y0: 0 },
                         { x:  10, y:  86, x0:   0, y0: 0 },
                         { x:  26, y:  54, x0:   0, y0: 0 }
        ],
        trackContainer:  { x:  19, y: 230, x0:   0, y0: 0 },
        trackMask:       { width: 322, height: 192},
        trackLength: 255,
        raceNotice:      { x: 180, y: 100, x0: 0.5, y0: 0, size: 39, text: "Race "}
    },
    results: {
        resultsOMatic:   { x:   6, y:   4, x0:   0, y0: 0 },
        buttonNext:      { x: 254, y: 549, x0:   0, y0: 0 },
        resultsText:     { x:  18, y:  16, x0:   0, y0: 0, size: 16},
        codeLength: 11
    },
    summary: {
        playAgain:       { x:  29, y: 370, x0:   0, y0: 0 },
        getReport:       { x: 183, y: 370, x0:   0, y0: 0 },
        thankYou:        { x: 180, y: 510, x0: 0.5, y0: 0 }
    },
    zzz: null
}

var gameStatus = {
    gamesetName: "",
    questions: [],
    currentQuestion: {},
    currentQuestionNumber: 0,
    runningTweens: 0,
    answers: [],
    correctNumber: 0,
    answersNumber: 0
}

/**************************************************************************/
/* Scenes definitions                                                     */
/* Scene: sceneMainMenu                                                       */
/**************************************************************************/
var sceneBoot = new Phaser.Scene('sceneBoot');

sceneBoot.preload = function () {
    // This part shows the progress bar
    let loadingText = this.make.text({
        x: gameSettings.width / 2,
        y: (gameSettings.height - gameSettings.progressBar.height) / 2 - gameSettings.progressBar.margin,
        text: 'Loading...',
        style: {
            font: gameSettings.progressBar.textStyle,
            fill: gameSettings.progressBar.textColor
        }});
    loadingText.setOrigin(0.5, 1);

    let loadingAsset = this.make.text({
        x: gameSettings.width / 2,
        y: (gameSettings.height + gameSettings.progressBar.height) / 2 + gameSettings.progressBar.margin,
        text: '',
        style: {
            font: gameSettings.progressBar.textStyle,
            fill: gameSettings.progressBar.textColor
        }});
    loadingAsset.setOrigin(0.5, 0);


    let progressBox = this.add.graphics();
    progressBox.fillStyle(0xffffff, 1);
    progressBox.fillRect((gameSettings.width - gameSettings.progressBar.width) / 2,
                         (gameSettings.height - gameSettings.progressBar.height) / 2,
                         gameSettings.progressBar.width,
                         gameSettings.progressBar.height);

    let progressBar = this.add.graphics();

    let loadingPercent = this.make.text({
        x: gameSettings.width / 2,
        y: gameSettings.height / 2,
        text: '',
        style: {
            font: gameSettings.progressBar.textStyle,
            fill: gameSettings.progressBar.textColor
        }});
    loadingPercent.setOrigin(0.5, 0.5);

    this.load.on('progress', function (value) {
        // This is to slow down the loading phase for debugging
        //let start = Date.now();
        //let now = start;
        //while (now - start < 1000) {
        //    now = Date.now();
        //}

        loadingPercent.setText(parseInt(value * 100) + '%');
        progressBar.clear();
        progressBar.fillStyle(gameSettings.progressBar.barColor, 1);
        progressBar.fillRect((gameSettings.width - gameSettings.progressBar.width + 2 * gameSettings.progressBar.padding) / 2,
                             (gameSettings.height - gameSettings.progressBar.height + 2 * gameSettings.progressBar.padding) / 2,
                             (gameSettings.progressBar.width - 2 * gameSettings.progressBar.padding) * value,
                             gameSettings.progressBar.height - 2 * gameSettings.progressBar.padding);
    });

    this.load.on('fileprogress', function (file) {
        loadingAsset.setText('Loading asset: ' + file.key);
    });

    this.load.on('complete', function () {
        progressBox.destroy();
        progressBar.destroy();
        loadingText.destroy();
        loadingPercent.destroy();
        loadingAsset.destroy();
    });

    this.load.atlas("atlas_main",
                    "assets/sprites/atlas_main.png",
                    "assets/sprites/atlas_main.json");
    this.load.bitmapFont("BitPotionExtWhite",
                         "assets/fonts/BitPotionExt_0.png",
                         "assets/fonts/BitPotionExt.xml");
    //this.load.bitmapFont("BitPotionExtBlack",
    //                     "assets/fonts/BitPotionExt_Black_0.png",
    //                     "assets/fonts/BitPotionExt_Black.xml");
    this.load.bitmapFont("pixel7x7",
                         "assets/fonts/pixel7x7.png",
                         "assets/fonts/pixel7x7.xml");
    this.load.bitmapFont("scoreboard",
                         "assets/fonts/scoreboard.png",
                         "assets/fonts/scoreboard.xml");
    //this.load.bitmapFont("keyboard",
    //                     "assets/fonts/keyboard.png",
    //                     "assets/fonts/keyboard.xml");
    this.load.json('gameset', 'assets/gamesets/default.json');
}
sceneBoot.create = function () {
    let gameset = this.cache.json.get('gameset');
    gameStatus.gamesetName = gameset.name;
    gameStatus.questions = gameset.questions;

    this.scene.start("sceneMenuMain");
}

/**************************************************************************/
/* Scene: sceneMenuMain                                                   */
/**************************************************************************/
var sceneMenuMain = new Phaser.Scene('sceneMenuMain');
sceneMenuMain.preload = function () {
    // Resetting the gameStatus object, to add a "Play again" button
    // and the game can start again from sceneMenuMain without having
    // to load all the assets again.
    gameStatus.currentQuestion = {};
    gameStatus.currentQuestionNumber = 0;
    gameStatus.runningTweens = 0;
    gameStatus.answers = [];
    gameStatus.correctNumber = 0;
    gameStatus.answersNumber = 0;
}
sceneMenuMain.create = function () {
    this.input.keyboard.on('keydown-S', function (event) {
        game.renderer.snapshot(function (image) {
            downloadFile('maggots-races_main.png', image.src, "image/png");
        });
    }, this);

    this.blackboard = this.add.sprite(0, 0, "atlas_main", "blackboard");
    this.blackboard.setOrigin(0, 0);

    this.titlegraphics = this.add.sprite(gameSettings.mainScreen.titlegraphics.x,
                                         gameSettings.mainScreen.titlegraphics.y,
                                         "atlas_main",
                                         "title");
    this.titlegraphics.setOrigin(gameSettings.mainScreen.titlegraphics.x0,
                                 gameSettings.mainScreen.titlegraphics.y0);

    this.buttonStart = this.add.sprite(gameSettings.mainScreen.buttonStart.x,
                                       gameSettings.mainScreen.buttonStart.y,
                                       "atlas_main",
                                       "button_start");
    this.buttonStart.setOrigin(gameSettings.mainScreen.buttonStart.x0,
                               gameSettings.mainScreen.buttonStart.y0);
    this.buttonStart.setInteractive();
    this.buttonStart.on("pointerdown", function(){
        this.scene.start("sceneLevelLoad");
    }, this);

    this.clickHere = this.add.sprite(gameSettings.mainScreen.clickHere.x,
                                     gameSettings.mainScreen.clickHere.y,
                                     "atlas_main",
                                     "click_here");
    this.clickHere.setOrigin(gameSettings.mainScreen.clickHere.x0,
                             gameSettings.mainScreen.clickHere.y0);

    // This is for future uses
    //this.buttonLevel = this.add.sprite(gameSettings.mainScreen.buttonLevel.x,
    //                                   gameSettings.mainScreen.buttonLevel.y,
    //                                   "atlas_main",
    //                                   "button_level");
    //this.buttonLevel.setOrigin(gameSettings.mainScreen.buttonLevel.x0,
    //                           gameSettings.mainScreen.buttonLevel.y0);
    //let keyboardText = "123456789 \n" +
    //                   "QWERTYU  P\n" +
    //                   " ASDFGHJKL\n" +
    //                   " ZXCVBNM\n" +
    //                   "";
    //this.keyboard = this.add.bitmapText(gameSettings.results.resultsText.x,
    //                                    gameSettings.results.resultsText.y,
    //                                    "keyboard", keyboardText,
    //                                    gameSettings.results.resultsText.size);
    //this.keyboard.setOrigin(gameSettings.results.resultsText.x0,
    //                        gameSettings.results.resultsText.y0);

    this.copyrightText = this.add.bitmapText(gameSettings.mainScreen.copyrightNotice.x,
                                             gameSettings.mainScreen.copyrightNotice.y,
                                             "BitPotionExtWhite", gameSettings.mainScreen.copyrightNotice.text,
                                             gameSettings.mainScreen.copyrightNotice.size);
    this.copyrightText.setOrigin(gameSettings.mainScreen.copyrightNotice.x0,
                                 gameSettings.mainScreen.copyrightNotice.y0);

    this.versionText = this.add.bitmapText(gameSettings.mainScreen.versionText.x,
                                           gameSettings.mainScreen.versionText.y,
                                           "BitPotionExtWhite", gameSettings.mainScreen.versionText.text,
                                           gameSettings.mainScreen.versionText.size,
                                           Phaser.GameObjects.BitmapText.ALIGN_RIGHT);
    this.versionText.setOrigin(gameSettings.mainScreen.versionText.x0,
                               gameSettings.mainScreen.versionText.y0);
}

/**************************************************************************/
/* Scene: sceneLevelLoad                                                  */
/**************************************************************************/
var sceneLevelLoad = new Phaser.Scene('sceneLevelLoad');
sceneLevelLoad.loadingQuestion = {
    spriteLoaded: false,
    winnerDetected: false,
    createFunction: false
};
sceneLevelLoad.checkPhases = function (phase) {
    // We have to start the next scene with this callback because
    // we load the question sprites from a base64 string.
    // This decoding is not managed by the image loader, thus the
    // preload() function will not wait until the image is loaded,
    // and we need to manually wait till the image has been loaded.
    if (phase === 'spriteLoaded') {
        this.loadingQuestion.spriteLoaded = true;
    } else if (phase === 'winnerDetected') {
        this.loadingQuestion.winnerDetected = true;
    } else if (phase === 'createFunction') {
        this.loadingQuestion.createFunction = true;
    }

    if (this.loadingQuestion.spriteLoaded === true &&
        this.loadingQuestion.winnerDetected === true &&
        this.loadingQuestion.createFunction === true) {

        this.scene.start("sceneQuestion");
    }
}
sceneLevelLoad.preload = function () {
    this.emitter = new Phaser.Events.EventEmitter();

    this.emitter.on('loadingQuestion', this.checkPhases, this);

    this.blackboard = this.add.sprite(0, 0, "atlas_main", "blackboard");
    this.blackboard.setOrigin(0, 0);

    this.copyrightText = this.add.bitmapText(gameSettings.mainScreen.copyrightNotice.x,
                                             gameSettings.mainScreen.copyrightNotice.y,
                                             "BitPotionExtWhite", gameSettings.mainScreen.copyrightNotice.text,
                                             gameSettings.mainScreen.copyrightNotice.size);
    this.copyrightText.setOrigin(gameSettings.mainScreen.copyrightNotice.x0,
                                 gameSettings.mainScreen.copyrightNotice.y0);

    this.loadingLevel = this.add.sprite(gameSettings.loadLevelScreen.loadingLevel.x,
                                        gameSettings.loadLevelScreen.loadingLevel.y,
                                        "atlas_main",
                                        "loading_level");
    this.loadingLevel.setOrigin(gameSettings.loadLevelScreen.loadingLevel.x0,
                                gameSettings.loadLevelScreen.loadingLevel.y0);

    // Analyzing and loading the current question
    gameStatus.currentQuestion = gameStatus.questions[gameStatus.currentQuestionNumber];

    var that = this;

    if (gameStatus.currentQuestion.imageType === "base64") {
        // Loading an image like this does not call the image loader,
        this.textures.addBase64('question', gameStatus.currentQuestion.image);

        // Loader to wait all base64 Image loaded
	this.textures.on('onload', function() { 
            that.emitter.emit('loadingQuestion', 'spriteLoaded');
	});
    } else {
        this.load.image("question", "assets/gamesets/" + gameStatus.currentQuestion.image);
        this.load.on('complete', function () {
            that.emitter.emit('loadingQuestion', 'spriteLoaded');
	});
    }

    // Determining the winner
    let arrivals = [];
    const trackLength = gameStatus.currentQuestion.trackLength;

    for (let i = 0; i < gameSettings.track.maggots.length; i+=1) {
        const positions = gameStatus.currentQuestion.positions[i];

        arrivals[i] = positions.length + 1;

        for (let j = 0; j < positions.length && arrivals[i] > positions.length; j+=1) {
            if (positions[j] >= trackLength) {
                arrivals[i] = j;
            }
        }
    }

    let winner = 0;
    for (let i = 1; i < arrivals.length; i++) {
        if (arrivals[i] < arrivals[winner]) {
            winner = i;
        }
    }

    gameStatus.currentQuestion.winner = winner;

    // Determining the code for the next level
    if (gameStatus.currentQuestionNumber < (gameStatus.questions.length - 1)) {
        gameStatus.currentQuestion.nextCode = gameStatus.questions[gameStatus.currentQuestionNumber + 1].code;
    } else {
        gameStatus.currentQuestion.nextCode = Array(gameSettings.results.codeLength).fill("_").join("");
    }

    this.emitter.emit('loadingQuestion', 'winnerDetected');
}
sceneLevelLoad.create = function () {
    this.emitter.emit('loadingQuestion', 'createFunction');
}

/**************************************************************************/
/* Scene: sceneQuestion                                                   */
/**************************************************************************/
var sceneQuestion = new Phaser.Scene('sceneQuestion');
sceneQuestion.chooseMaggot = function (chosenMaggot) {
    return function () {
        if (chosenMaggot === gameStatus.currentQuestion.winner) {
            gameStatus.correctNumber += 1;
        }

        gameStatus.answers[gameStatus.currentQuestionNumber] = {
            answer: chosenMaggot,
            winner: gameStatus.currentQuestion.winner,
            questionNumber: gameStatus.currentQuestionNumber,
            correct: (chosenMaggot === gameStatus.currentQuestion.winner)
        };

        gameStatus.answersNumber += 1;

        this.scene.start("sceneRace");
    }
}
sceneQuestion.preload = function () {
}
sceneQuestion.create = function () {
    this.input.keyboard.on('keydown-S', function (event) {
        game.renderer.snapshot(function (image) {
            downloadFile('maggots-races_question.png', image.src, "image/png");
        });
    }, this);

    this.blackboard = this.add.sprite(0, 0, "atlas_main", "blackboard");
    this.blackboard.setOrigin(0, 0);

    this.labelChoose = this.add.sprite(gameSettings.questionScreen.labelChoose.x,
                                       gameSettings.questionScreen.labelChoose.y,
                                       "atlas_main",
                                       "label_choose");
    this.labelChoose.setOrigin(gameSettings.questionScreen.labelChoose.x0,
                               gameSettings.questionScreen.labelChoose.y0);

    this.buttonsChoose = [];

    for (let i = 0; i < gameSettings.track.maggots.length; i+=1) {
        this.buttonsChoose[i] = this.add.sprite(gameSettings.questionScreen.buttonsChoose[i].x,
                                                gameSettings.questionScreen.buttonsChoose[i].y,
                                                "atlas_main",
                                                "button_choose" + (i + 1).toString());
        this.buttonsChoose[i].setOrigin(gameSettings.questionScreen.buttonsChoose[i].x0,
                                     gameSettings.questionScreen.buttonsChoose[i].y0);
        this.buttonsChoose[i].setInteractive();
        this.buttonsChoose[i].on("pointerdown", this.chooseMaggot(i), this);
    }

    this.copyrightText = this.add.bitmapText(gameSettings.mainScreen.copyrightNotice.x,
                                             gameSettings.mainScreen.copyrightNotice.y,
                                             "BitPotionExtWhite", gameSettings.mainScreen.copyrightNotice.text,
                                             gameSettings.mainScreen.copyrightNotice.size);
    this.copyrightText.setOrigin(gameSettings.mainScreen.copyrightNotice.x0,
                                 gameSettings.mainScreen.copyrightNotice.y0);

    this.question = this.add.image(gameSettings.questionScreen.question.x,
                                   gameSettings.questionScreen.question.y,
                                   "question");
    this.question.setOrigin(gameSettings.questionScreen.question.x0,
                            gameSettings.questionScreen.question.y0);

    this.questionText = this.add.bitmapText(gameSettings.questionScreen.questionText.x,
                                            gameSettings.questionScreen.questionText.y,
                                            "BitPotionExtWhite", gameSettings.questionScreen.questionText.text + (gameStatus.currentQuestionNumber + 1).toString(),
                                            gameSettings.questionScreen.questionText.size,
                                            Phaser.GameObjects.BitmapText.ALIGN_RIGHT);
    this.questionText.setOrigin(gameSettings.questionScreen.questionText.x0,
                                gameSettings.questionScreen.questionText.y0);

}

/**************************************************************************/
/* Scene: sceneRace                                                       */
/**************************************************************************/
var sceneRace = new Phaser.Scene('sceneRace');
sceneRace.doStartRace = function () {
    this.raceStart.destroy();

    gameStatus.runningTweens = gameSettings.track.maggots.length;

    const maxTime = gameStatus.currentQuestion.maxTime * 1000;

    const questionTrackLength = gameStatus.currentQuestion.trackLength;
    const screenTrackLength = gameSettings.track.trackLength;
    var raceText = this.raceText;

    for (let i = 0; i < gameSettings.track.maggots.length; i+=1) {
        const positions = gameStatus.currentQuestion.positions[i];
        const firstPosition =positions[0];
        const lastPosition = positions[positions.length - 1];
        const distance = lastPosition - firstPosition;

        // Normalizing and offsetting in order to have the first value always 0 and 
        // the last value always 1, because in the ease function 0 corresponds to the starting
        // poisition and 1 to the last position.
        const eases = positions.map(x => (x - firstPosition) / distance);

        // The actual last position is calculated here and converted to pixels, so it is different
        // from the eases data.
        const xScreen = gameSettings.track.maggots[i].x;
        const start = firstPosition / questionTrackLength * screenTrackLength + xScreen;
        const end = lastPosition / questionTrackLength * screenTrackLength + start;


        this.tweens.add({
            targets: this.maggots[i],
            x: end,
            duration: maxTime,
            ease: (t => Phaser.Math.Interpolation.CatmullRom(eases, t)),
            repeat: 0,
            onComplete: function(){
                gameStatus.runningTweens -= 1;

                if (gameStatus.runningTweens <= 0) {
                    raceText.setText(gameSettings.track.raceNotice.text + (gameStatus.currentQuestionNumber + 1).toString() +
                                     "\n\nMaggot " + (gameStatus.currentQuestion.winner + 1).toString() + " wins!!!");
                }
            }
        });
    }

    this.raceResults = this.add.sprite(gameSettings.track.raceStart.x,
                                       gameSettings.track.raceStart.y,
                                       "atlas_main", "race_results");
    this.raceResults.setOrigin(gameSettings.track.raceStart.x0,
                               gameSettings.track.raceStart.y0);
    this.raceResults.setInteractive();
    this.raceResults.on("pointerdown", function () {
        this.scene.start("sceneResults");
    }, this);
}
sceneRace.preload = function () {
}
sceneRace.create = function () {
    this.input.keyboard.on('keydown-S', function (event) {
        game.renderer.snapshot(function (image) {
            downloadFile('maggots-races_race.png', image.src, "image/png");
        });
    }, this);

    this.blackboard = this.add.sprite(0, 0, "atlas_main", "blackboard");
    this.blackboard.setOrigin(0, 0);
    this.screen = this.add.sprite(0, 0, "atlas_main", "screen");
    this.screen.setOrigin(0, 0);

    this.raceText = this.add.bitmapText(gameSettings.track.raceNotice.x,
                                        gameSettings.track.raceNotice.y,
                                        "pixel7x7", gameSettings.track.raceNotice.text + (gameStatus.currentQuestionNumber + 1).toString(),
                                        gameSettings.track.raceNotice.size, Phaser.GameObjects.BitmapText.ALIGN_CENTER);
    this.raceText.setOrigin(gameSettings.track.raceNotice.x0,
                            gameSettings.track.raceNotice.y0);

    this.trackContainer = this.add.container(gameSettings.track.trackContainer.x,
                                             gameSettings.track.trackContainer.y);

    this.trackShape = this.make.graphics();
    this.trackShape.fillStyle(0xffffff, 1);
    this.trackShape.fillRect(gameSettings.track.trackContainer.x,
                             gameSettings.track.trackContainer.y,
                             gameSettings.track.trackMask.width,
                             gameSettings.track.trackMask.height);
    this.trackMask = this.trackShape.createGeometryMask();
    this.trackContainer.setMask(this.trackMask);

    var advertisements = this.add.sprite(gameSettings.track.advertisements.x,
                                         gameSettings.track.advertisements.y,
                                         "atlas_main", "advertisements");
    advertisements.setOrigin(gameSettings.track.advertisements.x0,
                             gameSettings.track.advertisements.y0);
    var grassTop = this.add.sprite(gameSettings.track.grassTop.x,
                                   gameSettings.track.grassTop.y,
                                   "atlas_main", "grass");
    grassTop.setOrigin(gameSettings.track.grassTop.x0,
                       gameSettings.track.grassTop.y0);
    var grassBottom = this.add.sprite(gameSettings.track.grassBottom.x,
                                      gameSettings.track.grassBottom.y,
                                      "atlas_main", "grass");
    grassBottom.setOrigin(gameSettings.track.grassBottom.x0,
                          gameSettings.track.grassBottom.y0);
    var lane1 = this.add.sprite(gameSettings.track.lane1.x,
                                gameSettings.track.lane1.y,
                                "atlas_main", "lane");
    lane1.setOrigin(gameSettings.track.lane1.x0,
                    gameSettings.track.lane1.y0);
    var lane2 = this.add.sprite(gameSettings.track.lane2.x,
                                gameSettings.track.lane2.y,
                                "atlas_main", "lane");
    lane2.setOrigin(gameSettings.track.lane2.x0,
                    gameSettings.track.lane2.y0);
    var lane3 = this.add.sprite(gameSettings.track.lane3.x,
                                gameSettings.track.lane3.y,
                                "atlas_main", "lane");
    lane3.setOrigin(gameSettings.track.lane3.x0,
                    gameSettings.track.lane3.y0);

    var laneNumber1a = this.add.sprite(gameSettings.track.laneNumber1a.x,
                                       gameSettings.track.laneNumber1a.y,
                                       "atlas_main", "lane_number_1");
    laneNumber1a.setOrigin(gameSettings.track.laneNumber1a.x0,
                           gameSettings.track.laneNumber1a.y0);
    var laneNumber1b = this.add.sprite(gameSettings.track.laneNumber1b.x,
                                       gameSettings.track.laneNumber1b.y,
                                       "atlas_main", "lane_number_1");
    laneNumber1b.setOrigin(gameSettings.track.laneNumber1b.x0,
                           gameSettings.track.laneNumber1b.y0);
    var laneNumber2a = this.add.sprite(gameSettings.track.laneNumber2a.x,
                                       gameSettings.track.laneNumber2a.y,
                                       "atlas_main", "lane_number_2");
    laneNumber2a.setOrigin(gameSettings.track.laneNumber2a.x0,
                           gameSettings.track.laneNumber2a.y0);
    var laneNumber2b = this.add.sprite(gameSettings.track.laneNumber2b.x,
                                       gameSettings.track.laneNumber2b.y,
                                       "atlas_main", "lane_number_2");
    laneNumber2b.setOrigin(gameSettings.track.laneNumber2b.x0,
                           gameSettings.track.laneNumber2b.y0);
    var laneNumber3a = this.add.sprite(gameSettings.track.laneNumber3a.x,
                                       gameSettings.track.laneNumber3a.y,
                                       "atlas_main", "lane_number_3");
    laneNumber3a.setOrigin(gameSettings.track.laneNumber3a.x0,
                           gameSettings.track.laneNumber3a.y0);
    var laneNumber3b = this.add.sprite(gameSettings.track.laneNumber3b.x,
                                       gameSettings.track.laneNumber3b.y,
                                       "atlas_main", "lane_number_3");
    laneNumber3b.setOrigin(gameSettings.track.laneNumber3b.x0,
                           gameSettings.track.laneNumber3b.y0);

    var toBeAdded = [advertisements,
                     grassTop, grassBottom,
                     lane1, lane2, lane3,
                     laneNumber1a, laneNumber1b,
                     laneNumber2a, laneNumber2b,
                     laneNumber3a, laneNumber3b];

    const questionTrackLength = gameStatus.currentQuestion.trackLength;
    const screenTrackLength = gameSettings.track.trackLength;

    this.maggots = [];

    for (let i = 0; i < gameSettings.track.maggots.length; i+=1) {
        const firstPosition = gameStatus.currentQuestion.positions[i][0];
        const xScreen = gameSettings.track.maggots[i].x;
        const x = firstPosition / questionTrackLength * screenTrackLength + xScreen;

        this.maggots[i] = this.add.sprite(x,
                                          gameSettings.track.maggots[i].y,
                                          "atlas_main", "maggot");
        this.maggots[i].setOrigin(gameSettings.track.maggots[i].x0,
                                  gameSettings.track.maggots[i].y0);

        toBeAdded.push(this.maggots[i]);
    }

    this.trackContainer.add(toBeAdded);

    this.raceStart = this.add.sprite(gameSettings.track.raceStart.x,
                                     gameSettings.track.raceStart.y,
                                     "atlas_main", "race_start");
    this.raceStart.setOrigin(gameSettings.track.raceStart.x0,
                             gameSettings.track.raceStart.y0);
    this.raceStart.setInteractive();
    this.raceStart.on("pointerdown", this.doStartRace, this);
}

/**************************************************************************/
/* Scene: sceneResults                                                    */
/**************************************************************************/
var sceneResults = new Phaser.Scene('sceneResults');
sceneResults.nextQuestion = function () {
    if (gameStatus.currentQuestionNumber < (gameStatus.questions.length - 1)) {
        gameStatus.currentQuestionNumber += 1;
        this.scene.start("sceneLevelLoad");
    } else {
        gameStatus.currentQuestionNumber = 0;
        this.scene.start("sceneSummary");
    }
}
sceneResults.preload = function () {
}
sceneResults.create = function () {
    this.input.keyboard.on('keydown-S', function (event) {
        game.renderer.snapshot(function (image) {
            downloadFile('maggots-races_results.png', image.src, "image/png");
        });
    }, this);
    // This is cheating! Used for debugging
    //this.input.keyboard.on('keydown-F', function (event) {
    //    this.scene.start("sceneSummary");
    //}, this);

    this.blackboard = this.add.sprite(0, 0, "atlas_main", "blackboard");
    this.blackboard.setOrigin(0, 0);
    this.copyrightText = this.add.bitmapText(gameSettings.mainScreen.copyrightNotice.x,
                                             gameSettings.mainScreen.copyrightNotice.y,
                                             "BitPotionExtWhite", gameSettings.mainScreen.copyrightNotice.text,
                                             gameSettings.mainScreen.copyrightNotice.size);
    this.copyrightText.setOrigin(gameSettings.mainScreen.copyrightNotice.x0,
                                 gameSettings.mainScreen.copyrightNotice.y0);

    this.resultsOMatic = this.add.sprite(gameSettings.results.resultsOMatic.x,
                                         gameSettings.results.resultsOMatic.y,
                                         "atlas_main",
                                         "results-o-matic");
    this.resultsOMatic.setOrigin(gameSettings.results.resultsOMatic.x0,
                                 gameSettings.results.resultsOMatic.y0);

    this.buttonNext = this.add.sprite(gameSettings.results.buttonNext.x,
                                      gameSettings.results.buttonNext.y,
                                      "atlas_main",
                                      "button_next");
    this.buttonNext.setOrigin(gameSettings.results.buttonNext.x0,
                              gameSettings.results.buttonNext.y0);
    this.buttonNext.setInteractive();
    this.buttonNext.on("pointerdown", this.nextQuestion, this);

    let percent = (gameStatus.correctNumber / gameStatus.answersNumber * 100).toFixed(0);

    let text = "             \n" +
               " RESULTS____ \n" +
               "             \n" +
               " Race_n__" + zeroPad(gameStatus.currentQuestionNumber + 1, 3) + "\n" +
               " Winner__" + (gameStatus.currentQuestion.winner + 1).toString() + "!_ \n" +
               "             \n" +
               "             \n" +
               " ANSWERS____ \n" +
               "             \n" +
               " Correct_" + zeroPad(gameStatus.correctNumber, 3) + "\n" +
               " Total___" + zeroPad(gameStatus.answersNumber, 3) + "\n" +
               " _______" + zeroPad(percent, 3) + "% \n" +
               "             \n" +
               "             \n" +
               " " + gameStatus.currentQuestion.nextCode + " \n" +
               "";

    this.resultsText = this.add.bitmapText(gameSettings.results.resultsText.x,
                                           gameSettings.results.resultsText.y,
                                           "scoreboard", text,
                                           gameSettings.results.resultsText.size);
    this.resultsText.setOrigin(gameSettings.results.resultsText.x0,
                               gameSettings.results.resultsText.y0);

    this.textures.remove('question');
}

/**************************************************************************/
/* Scene: sceneSummary                                                    */
/**************************************************************************/
var sceneSummary = new Phaser.Scene('sceneSummary');
sceneSummary.generateReport = function () {
    let report = {};
    report.n = gameStatus.gamesetName.toString();
    report.a = gameStatus.answers.map(a => (a.correct ? 1 : 0));
    report.p = (gameStatus.correctNumber / gameStatus.answersNumber * 100).toFixed(2);
    report.t = (new Date()).toISOString();
    report.v = gameSettings.mainScreen.versionText.text;

    const str = report.n + report.a.join("") + report.p + report.t + report.v;

    report.h = hashCode(str);

    const reportJSON = JSON.stringify(report);

    let canvas = document.createElement('canvas');
    try {
        // The return value is the canvas element
        bwipjs.toCanvas(canvas, {
            bcid:   'azteccode',
            text:   reportJSON,
            //format: "full",
            scale:  3
        });

        let ctx = canvas.getContext("2d");
        ctx.globalCompositeOperation = 'destination-over';
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        downloadFile("maggots-races_report.png", canvas.toDataURL('image/png'), "image/png");
    } catch (e) {
        console.error(e);
    }
}
sceneSummary.preload = function () {
}
sceneSummary.create = function () {
    this.input.keyboard.on('keydown-S', function (event) {
        game.renderer.snapshot(function (image) {
            downloadFile('maggots-races_summary.png', image.src, "image/png");
        });
    }, this);

    this.blackboard = this.add.sprite(0, 0, "atlas_main", "blackboard");
    this.blackboard.setOrigin(0, 0);
    this.copyrightText = this.add.bitmapText(gameSettings.mainScreen.copyrightNotice.x,
                                             gameSettings.mainScreen.copyrightNotice.y,
                                             "BitPotionExtWhite", gameSettings.mainScreen.copyrightNotice.text,
                                             gameSettings.mainScreen.copyrightNotice.size);
    this.copyrightText.setOrigin(gameSettings.mainScreen.copyrightNotice.x0,
                                 gameSettings.mainScreen.copyrightNotice.y0);

    this.resultsOMatic = this.add.sprite(gameSettings.results.resultsOMatic.x,
                                         gameSettings.results.resultsOMatic.y,
                                         "atlas_main",
                                         "results-o-matic");
    this.resultsOMatic.setOrigin(gameSettings.results.resultsOMatic.x0,
                                 gameSettings.results.resultsOMatic.y0);

    const percent = (gameStatus.correctNumber / gameStatus.answersNumber * 100).toFixed(0);

    let message = "___________";

    if (percent >= 100) {
        message = "PERFECT!!!_";
    } else if (percent >= 60) {
        message = "WELL_DONE!_";
    }

    let text = "             \n" +
               " _GAME_OVER_ \n" +
               "             \n" +
               " ANSWERS____ \n" +
               "             \n" +
               " Correct_" + zeroPad(gameStatus.correctNumber, 3) + "\n" +
               " Total___" + zeroPad(gameStatus.answersNumber, 3) + "\n" +
               " _______" + zeroPad(percent, 3) + "% \n" +
               "             \n" +
               " " + message + " \n" +
               "";

    this.resultsText = this.add.bitmapText(gameSettings.results.resultsText.x,
                                           gameSettings.results.resultsText.y,
                                           "scoreboard", text,
                                           gameSettings.results.resultsText.size);
    this.resultsText.setOrigin(gameSettings.results.resultsText.x0,
                               gameSettings.results.resultsText.y0);

    this.thankYou = this.add.sprite(gameSettings.summary.thankYou.x,
                                    gameSettings.summary.thankYou.y,
                                    "atlas_main",
                                    "thank_you_note");
    this.thankYou.setOrigin(gameSettings.summary.thankYou.x0,
                            gameSettings.summary.thankYou.y0);

    this.playAgain = this.add.sprite(gameSettings.summary.playAgain.x,
                                     gameSettings.summary.playAgain.y,
                                     "atlas_main",
                                     "play_again");
    this.playAgain.setOrigin(gameSettings.summary.playAgain.x0,
                             gameSettings.summary.playAgain.y0);
    this.playAgain.setInteractive();
    this.playAgain.on("pointerdown", function(){
        this.scene.start("sceneMenuMain");
    }, this);

    this.getReport = this.add.sprite(gameSettings.summary.getReport.x,
                                     gameSettings.summary.getReport.y,
                                     "atlas_main",
                                     "get_report");
    this.getReport.setOrigin(gameSettings.summary.getReport.x0,
                             gameSettings.summary.getReport.y0);
    this.getReport.setInteractive();
    this.getReport.on("pointerdown", this.generateReport, this);
}

/**************************************************************************/
/* Phaser initiation                                                      */
/**************************************************************************/
const gameConfig = {
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: "game_location",
        width: gameSettings.width,
        height: gameSettings.height
    },
    render: {
        pixelArt: true
    },
    transparent: true,
    backgroundColor: gameSettings.background,
    scene: [sceneBoot, sceneMenuMain, sceneLevelLoad, sceneQuestion, sceneRace, sceneResults, sceneSummary]
}

window.onload = function() {
    let elem = document.querySelector('#JS-warning');
    elem.parentNode.removeChild(elem);

    game = new Phaser.Game(gameConfig);
    window.focus();
}
