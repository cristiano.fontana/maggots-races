# Changelog

## [0.3.0] - 2021-02-15
### Added
- Added a "get report" button on the summary screen.

## [0.2.1] - 2021-02-14
### Added
- Displaying version number on the main screen.

## [0.2.0] - 2021-02-14
### Added
- A "play again" button on the summary screen.
- Promotional images.
- A "keyboard" bitmap font for future uses.

### Changed
- Question images are now embedded in the gameset JSON. Gamesets can
now be distributed as a single JSON file.
- The default gameset JSON is generated with the main build script.

## [0.1.0] - 2021-02-11
First playable version!!!

[0.2.1]: https://gitlab.com/cristiano.fontana/maggots-races/-/tags/v0.2.1
[0.2.0]: https://gitlab.com/cristiano.fontana/maggots-races/-/tags/v0.2.0
[0.1.0]: https://gitlab.com/cristiano.fontana/maggots-races/-/tags/v0.1.0
