"use strict";

const fs = require('fs');
const child_process = require('child_process');
const texturePacker = require("free-tex-packer-core");
const imagemin = require("imagemin");
const imageminPngquant = require("imagemin-pngquant");
const terser = require("terser");

const MINIFY = true;
const BUILD_FOLDER = './build/'
console.log("Using build folder: " + BUILD_FOLDER);

if (MINIFY) {
    console.log("Minifing game.js");

    const minifyOptions = {
        toplevel: true,
        sourceMap: false
    };

    terser.minify(fs.readFileSync("./src/game.js").toString(), minifyOptions).then(res => {
        fs.writeFileSync("public/js/game.js", res.code);
    }).catch(err => {
        console.log("Minifing error: " + err);
    });
} else {
    console.log("Copying game.js");

    fs.copyFileSync('src/game.js', 'public/js/game.js');
}

if (MINIFY) {
    console.log("Copying minified Phaser to the ./public/js/ folder");
    fs.copyFileSync('node_modules/phaser/dist/phaser.min.js', 'public/js/phaser.js');
    console.log("Copying minified bwip-js to the ./public/js/ folder");
    fs.copyFileSync('node_modules/bwip-js/dist/bwip-js-min.js', 'public/js/bwip-js.js');
} else {
    console.log("Copying Phaser to the ./public/js/ folder");
    fs.copyFileSync('node_modules/phaser/dist/phaser.js', 'public/js/phaser.js');
    console.log("Copying minified bwip-js to the ./public/js/ folder");
    fs.copyFileSync('node_modules/bwip-js/dist/bwip-js.js', 'public/js/bwip-js.js');
}

console.log("Creating the texture atlas");
const packerOptions = {
    //width: 1024,
    //height: 1024,
    fixedSize: false,
    padding: 0,
    extrude: 0,
    allowRotation: false,
    allowTrim: false,
    detectIdentical: true,
    removeFileExtension: true,
    prependFolderName: false,
    textureFormat: "png",
    scale: 1,
    tinify: false,
    packer: "OptimalPacker",
    exporter: "Phaser3",
    textureName: "atlas_main"
};

var image_paths = [];
image_paths.push("./assets/blackboard.png");
image_paths.push("./assets/title.png");
image_paths.push("./assets/click_here.png");
image_paths.push("./assets/button_start.png");
image_paths.push("./assets/button_level.png");
image_paths.push("./assets/button_language.png");
image_paths.push("./assets/button_gameset.png");
image_paths.push("./assets/copyright_notice.png");
image_paths.push("./assets/loading_level.png");
image_paths.push("./assets/label_choose.png");
image_paths.push("./assets/button_choose1.png");
image_paths.push("./assets/button_choose2.png");
image_paths.push("./assets/button_choose3.png");
image_paths.push("./assets/lane.png");
image_paths.push("./assets/lane_number_0.png");
image_paths.push("./assets/lane_number_1.png");
image_paths.push("./assets/lane_number_2.png");
image_paths.push("./assets/lane_number_3.png");
image_paths.push("./assets/lane_number_4.png");
image_paths.push("./assets/lane_number_5.png");
image_paths.push("./assets/lane_number_6.png");
image_paths.push("./assets/lane_number_7.png");
image_paths.push("./assets/lane_number_8.png");
image_paths.push("./assets/lane_number_9.png");
image_paths.push("./assets/advertisements.png");
image_paths.push("./assets/race_start.png");
image_paths.push("./assets/race_results.png");
image_paths.push("./assets/maggot.png");
image_paths.push("./assets/grass.png");
image_paths.push("./assets/screen.png");
image_paths.push("./assets/results-o-matic.png");
image_paths.push("./assets/button_next.png");
image_paths.push("./assets/thank_you_note.png");
image_paths.push("./assets/play_again.png");
image_paths.push("./assets/get_report.png");

var images = image_paths.map((p, i) => ({path: p, contents: fs.readFileSync(p)}));

texturePacker(images, packerOptions, (files, error) => {
    if (error) {
        console.error('Packaging failed', error);
    } else {
        for(let item of files) {
            console.log("Writing item: " + item.name + " to " + BUILD_FOLDER);
            fs.writeFileSync(BUILD_FOLDER + item.name, item.buffer);
        }

        console.log("Optimizing atlas image");
        imagemin([BUILD_FOLDER + "atlas_main.png"], {
            destination: "./public/assets/sprites/",
            plugins: [imageminPngquant({quality: [0.6, 0.7]})]
        });

        console.log("Minifing atlas JSON");
        fs.writeFileSync("./public/assets/sprites/atlas_main.json",
            JSON.stringify(
                JSON.parse(
                    fs.readFileSync(BUILD_FOLDER + "atlas_main.json"))));
    }
});

console.log("Generating the default gameset JSON");
child_process.exec('node generate_default.js',
    {cwd: './assets/gameset_default/'},
    function (err, stdout, stderr) {
        if (err) {
            throw err;
        }

        console.log("Minifing default gameset JSON");
        fs.writeFileSync("./public/assets/gamesets/default.json",
            JSON.stringify(
                JSON.parse(
                    fs.readFileSync("./build/gameset_default.json"))));
    }
);

var fonts = [];
fonts.push({name: "pixel7x7", pathPNG: "./assets/pixel7x7.png", pathXML: "./assets/pixel7x7.xml", destXML: "./public/assets/fonts/pixel7x7.xml"});
fonts.push({name: "scoreboard", pathPNG: "./assets/scoreboard.png", pathXML: "./assets/scoreboard.xml", destXML: "./public/assets/fonts/scoreboard.xml"});
fonts.push({name: "keyboard", pathPNG: "./assets/keyboard.png", pathXML: "./assets/keyboard.xml", destXML: "./public/assets/fonts/keyboard.xml"});

for (let i = 0; i < fonts.length; i++) {
    console.log("Optimizing " + fonts[i].name + " image");
    imagemin([fonts[i].pathPNG], {
        destination: "./public/assets/fonts/",
        plugins: [imageminPngquant({quality: [0.5, 0.6]})]
    });

    console.log("Copying " + fonts[i].name + " XML file");
    fs.copyFileSync(fonts[i].pathXML, fonts[i].destXML);
}
